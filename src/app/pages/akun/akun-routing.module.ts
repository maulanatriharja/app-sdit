import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AkunPage } from './akun.page';

const routes: Routes = [
  {
    path: '',
    component: AkunPage
  },
  {
    path: 'password',
    loadChildren: () => import('./password/password.module').then( m => m.PasswordPageModule)
  },
  {
    path: 'profil',
    loadChildren: () => import('./profil/profil.module').then( m => m.ProfilPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AkunPageRoutingModule {}
