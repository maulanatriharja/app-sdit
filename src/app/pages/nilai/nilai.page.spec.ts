import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NilaiPage } from './nilai.page';

describe('NilaiPage', () => {
  let component: NilaiPage;
  let fixture: ComponentFixture<NilaiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NilaiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NilaiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
