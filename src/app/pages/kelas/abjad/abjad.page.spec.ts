import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AbjadPage } from './abjad.page';

describe('AbjadPage', () => {
  let component: AbjadPage;
  let fixture: ComponentFixture<AbjadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbjadPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AbjadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
