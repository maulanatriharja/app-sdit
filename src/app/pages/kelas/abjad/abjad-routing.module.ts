import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AbjadPage } from './abjad.page';

const routes: Routes = [
  {
    path: '',
    component: AbjadPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AbjadPageRoutingModule {}
