import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AbjadPageRoutingModule } from './abjad-routing.module';

import { AbjadPage } from './abjad.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AbjadPageRoutingModule
  ],
  declarations: [AbjadPage]
})
export class AbjadPageModule {}
