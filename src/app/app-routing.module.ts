import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CheckTutorial } from './providers/check-tutorial.service';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/tutorial',
    pathMatch: 'full'
  },
  {
    path: 'account',
    loadChildren: () => import('./pages/account/account.module').then(m => m.AccountModule)
  },
  {
    path: 'support',
    loadChildren: () => import('./pages/support/support.module').then(m => m.SupportModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./pages/signup/signup.module').then(m => m.SignUpModule)
  },
  {
    path: 'app',
    loadChildren: () => import('./pages/tabs-page/tabs-page.module').then(m => m.TabsModule)
  },
  {
    path: 'tutorial',
    loadChildren: () => import('./pages/tutorial/tutorial.module').then(m => m.TutorialModule),
    canLoad: [CheckTutorial]
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'siswa',
    loadChildren: () => import('./pages/siswa/siswa.module').then( m => m.SiswaPageModule)
  },
  {
    path: 'akun',
    loadChildren: () => import('./pages/akun/akun.module').then( m => m.AkunPageModule)
  },
  {
    path: 'detail',
    loadChildren: () => import('./pages/guru/detail/detail.module').then( m => m.DetailPageModule)
  },
  {
    path: 'detail',
    loadChildren: () => import('./pages/pelajaran/detail/detail.module').then( m => m.DetailPageModule)
  }, 
  {
    path: 'kelas',
    loadChildren: () => import('./pages/kelas/kelas.module').then( m => m.KelasPageModule)
  },
  {
    path: 'nilai',
    loadChildren: () => import('./pages/nilai/nilai.module').then( m => m.NilaiPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}